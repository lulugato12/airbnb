defmodule Airbnb do
  require Integer
  require Float
  def aggr_count_properties(file) do
    File.stream!(file)
    |> CSV.decode!(headers: true)
    |> Enum.map(fn p -> {p["neighbourhood_cleansed"], p["property_type"]} end)
    |> Enum.frequencies
    |> Enum.map(fn {{n, p}, freq} -> %{colonia: n, tipo: p, total: freq} end)
  end

  def offer_by_neighbourhood(file) do
    File.stream!(file)
    |> CSV.decode!(headers: true)
    |> Enum.group_by(fn p -> p["neighbourhood_cleansed"] end)
    |> Enum.map(fn {col, rows} -> {col, Enum.reduce(rows, 0, fn x, acc -> acc + elem(Integer.parse(x["accommodates"]), 0) end), rows} end)
    |> Enum.map(fn {col, personas, rows} -> %{colonia: col, capacidad_de_hospedaje: personas, precio_promedio_por_persona: Enum.reduce(rows, 0, fn x, acc -> acc + elem(Float.parse(String.replace(String.trim(x["price"], "$"), ",", "")), 0) end)/personas} end)
  end
end
